diff --git a/lib/pomp_lib.h b/lib/pomp_lib.h
index f692304..d088c14 100644
--- a/lib/pomp_lib.h
+++ b/lib/pomp_lib.h
@@ -7,12 +7,12 @@
 #ifndef POMP_LIB_H
 #define POMP_LIB_H
 
-#include "opari_omp.h"
-
 #ifdef __cplusplus
 extern "C" {
 #endif
 
+#include <omp.h>
+
 struct ompregdescr {
   char* name;                  /* name of construct                     */
   char* sub_name;              /* optional: region name                 */
