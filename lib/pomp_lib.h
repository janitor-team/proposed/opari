/*************************************************************************/
/* OPARI Version 1.1                                                     */
/* Copyright (C) 2001                                                    */
/* Forschungszentrum Juelich, Zentralinstitut fuer Angewandte Mathematik */
/*************************************************************************/

#ifndef POMP_LIB_H
#define POMP_LIB_H

#include "opari_omp.h"

#ifdef __cplusplus
extern "C" {
#endif

struct ompregdescr {
  char* name;                  /* name of construct                     */
  char* sub_name;              /* optional: region name                 */
  int   num_sections;          /* sections only: number of sections     */
  char* file_name;             /* source code location                  */
  int   begin_first_line;      /* line number first line opening pragma */
  int   begin_last_line;       /* line number last  line opening pragma */
  int   end_first_line;        /* line number first line closing pragma */
  int   end_last_line;         /* line number last  line closing pragma */
  void* data;                  /* space for performance data            */
  struct ompregdescr* next;    /* for linking                           */
};

extern int POMP_MAX_ID;

extern struct ompregdescr* pomp_rd_table[];

extern void pomp_finalize();
extern void pomp_init();
extern void pomp_off();
extern void pomp_on();
extern void pomp_atomic_enter(struct ompregdescr* r);
extern void pomp_atomic_exit(struct ompregdescr* r);
extern void pomp_barrier_enter(struct ompregdescr* r);
extern void pomp_barrier_exit(struct ompregdescr* r);
extern void pomp_flush_enter(struct ompregdescr* r);
extern void pomp_flush_exit(struct ompregdescr* r);
extern void pomp_critical_begin(struct ompregdescr* r);
extern void pomp_critical_end(struct ompregdescr* r);
extern void pomp_critical_enter(struct ompregdescr* r);
extern void pomp_critical_exit(struct ompregdescr* r);
extern void pomp_for_enter(struct ompregdescr* r);
extern void pomp_for_exit(struct ompregdescr* r);
extern void pomp_master_begin(struct ompregdescr* r);
extern void pomp_master_end(struct ompregdescr* r);
extern void pomp_parallel_begin(struct ompregdescr* r);
extern void pomp_parallel_end(struct ompregdescr* r);
extern void pomp_parallel_fork(struct ompregdescr* r);
extern void pomp_parallel_join(struct ompregdescr* r);
extern void pomp_section_begin(struct ompregdescr* r);
extern void pomp_section_end(struct ompregdescr* r);
extern void pomp_sections_enter(struct ompregdescr* r);
extern void pomp_sections_exit(struct ompregdescr* r);
extern void pomp_single_begin(struct ompregdescr* r);
extern void pomp_single_end(struct ompregdescr* r);
extern void pomp_single_enter(struct ompregdescr* r);
extern void pomp_single_exit(struct ompregdescr* r);
extern void pomp_workshare_enter(struct ompregdescr* r);
extern void pomp_workshare_exit(struct ompregdescr* r);
extern void pomp_begin(struct ompregdescr* r);
extern void pomp_end(struct ompregdescr* r);

extern void pomp_init_lock(omp_lock_t *s);
extern void pomp_destroy_lock(omp_lock_t *s);
extern void pomp_set_lock(omp_lock_t *s);
extern void pomp_unset_lock(omp_lock_t *s);
extern int  pomp_test_lock(omp_lock_t *s);
extern void pomp_init_nest_lock(omp_nest_lock_t *s);
extern void pomp_destroy_nest_lock(omp_nest_lock_t *s);
extern void pomp_set_nest_lock(omp_nest_lock_t *s);
extern void pomp_unset_nest_lock(omp_nest_lock_t *s);
extern int  pomp_test_nest_lock(omp_nest_lock_t *s);

extern int pomp_tracing;

#ifdef __cplusplus
}
#endif

#endif
