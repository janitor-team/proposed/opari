/*************************************************************************/
/* OPARI Version 1.1                                                     */
/* Copyright (C) 2001                                                    */
/* Forschungszentrum Juelich, Zentralinstitut fuer Angewandte Mathematik */
/*************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#include "pomp_lib.h"

/*
 * Fortan subroutine external name setup
 */

#define pomp_finalize_U		POMP_FINALIZE
#define pomp_init_U		POMP_INIT
#define pomp_off_U		POMP_OFF
#define pomp_on_U		POMP_ON
#define pomp_atomic_enter_U	POMP_ATOMIC_ENTER
#define pomp_atomic_exit_U	POMP_ATOMIC_EXIT
#define pomp_barrier_enter_U	POMP_BARRIER_ENTER
#define pomp_barrier_exit_U	POMP_BARRIER_EXIT
#define pomp_flush_enter_U	POMP_FLUSH_ENTER
#define pomp_flush_exit_U	POMP_FLUSH_EXIT
#define pomp_critical_begin_U	POMP_CRITICAL_BEGIN
#define pomp_critical_end_U	POMP_CRITICAL_END
#define pomp_critical_enter_U	POMP_CRITICAL_ENTER
#define pomp_critical_exit_U	POMP_CRITICAL_EXIT
#define pomp_do_enter_U		POMP_DO_ENTER
#define pomp_do_exit_U		POMP_DO_EXIT
#define pomp_master_begin_U	POMP_MASTER_BEGIN
#define pomp_master_end_U	POMP_MASTER_END
#define pomp_parallel_begin_U	POMP_PARALLEL_BEGIN
#define pomp_parallel_end_U	POMP_PARALLEL_END
#define pomp_parallel_fork_U	POMP_PARALLEL_FORK
#define pomp_parallel_join_U	POMP_PARALLEL_JOIN
#define pomp_section_begin_U	POMP_SECTION_BEGIN
#define pomp_section_end_U	POMP_SECTION_END
#define pomp_sections_enter_U	POMP_SECTIONS_ENTER
#define pomp_sections_exit_U	POMP_SECTIONS_EXIT
#define pomp_single_begin_U	POMP_SINGLE_BEGIN
#define pomp_single_end_U	POMP_SINGLE_END
#define pomp_single_enter_U	POMP_SINGLE_ENTER
#define pomp_single_exit_U	POMP_SINGLE_EXIT
#define pomp_workshare_enter_U	POMP_WORKSHARE_ENTER
#define pomp_workshare_exit_U	POMP_WORKSHARE_EXIT
#define pomp_begin_U		POMP_BEGIN
#define pomp_end_U		POMP_END

#define XSUFFIX(name)  name##_
#define XSUFFIX2(name) name##__
#define XPREFIX(name)  _##name
#define XPREFIX2(name) __##name

#define SUFFIX(name)  XSUFFIX(name)
#define SUFFIX2(name) XSUFFIX2(name)
#define PREFIX(name)  XPREFIX(name)
#define PREFIX2(name) XPREFIX2(name)

#define UPCASE(name)  name##_U

#include "pomp_fwrapper_def.h"

/* --
#define FSUB(name) SUFFIX(name)
#define FSUB(name) SUFFIX2(name)
#define FSUB(name) PREFIX(name)
#define FSUB(name) PREFIX2(name)
#define FSUB(name) PREFIX(SUFFIX(name))

#define FSUB(name) UPCASE(name)
#define FSUB(name) SUFFIX(UPCASE(name))
#define FSUB(name) SUFFIX2(UPCASE(name))
#define FSUB(name) PREFIX(UPCASE(name))
#define FSUB(name) PREFIX2(UPCASE(name))
#define FSUB(name) PREFIX(SUFFIX(UPCASE(name)))
-- */

/*
 * Fortran wrappers calling the C versions
 */

void FSUB(pomp_finalize)() {
  pomp_finalize();
}

void FSUB(pomp_init)() {
  pomp_init();
}

void FSUB(pomp_off)() {
  pomp_tracing = 0;
}

void FSUB(pomp_on)() {
  pomp_tracing = 1;
}

void FSUB(pomp_atomic_enter)(int* id) {
  if ( pomp_tracing ) pomp_atomic_enter(pomp_rd_table[*id]);
}

void FSUB(pomp_atomic_exit)(int* id) {
  if ( pomp_tracing ) pomp_atomic_exit(pomp_rd_table[*id]);
}

void FSUB(pomp_barrier_enter)(int* id) {
  if ( pomp_tracing ) pomp_barrier_enter(pomp_rd_table[*id]);
}

void FSUB(pomp_barrier_exit)(int* id) {
  if ( pomp_tracing ) pomp_barrier_exit(pomp_rd_table[*id]);
}

void FSUB(pomp_flush_enter)(int* id) {
  if ( pomp_tracing ) pomp_flush_enter(pomp_rd_table[*id]);
}

void FSUB(pomp_flush_exit)(int* id) {
  if ( pomp_tracing ) pomp_flush_exit(pomp_rd_table[*id]);
}

void FSUB(pomp_critical_begin)(int* id) {
  if ( pomp_tracing ) pomp_critical_begin(pomp_rd_table[*id]);
}

void FSUB(pomp_critical_end)(int* id) {
  if ( pomp_tracing ) pomp_critical_end(pomp_rd_table[*id]);
}

void FSUB(pomp_critical_enter)(int* id) {
  if ( pomp_tracing ) pomp_critical_enter(pomp_rd_table[*id]);
}

void FSUB(pomp_critical_exit)(int* id) {
  if ( pomp_tracing ) pomp_critical_exit(pomp_rd_table[*id]);
}

void FSUB(pomp_do_enter)(int* id) {
  if ( pomp_tracing ) pomp_for_enter(pomp_rd_table[*id]);
}

void FSUB(pomp_do_exit)(int* id) {
  if ( pomp_tracing ) pomp_for_exit(pomp_rd_table[*id]);
}

void FSUB(pomp_master_begin)(int* id) {
  if ( pomp_tracing ) pomp_master_begin(pomp_rd_table[*id]);
}

void FSUB(pomp_master_end)(int* id) {
  if ( pomp_tracing ) pomp_master_end(pomp_rd_table[*id]);
}

void FSUB(pomp_parallel_begin)(int* id) {
  if ( pomp_tracing ) pomp_parallel_begin(pomp_rd_table[*id]);
}

void FSUB(pomp_parallel_end)(int* id) {
  if ( pomp_tracing ) pomp_parallel_end(pomp_rd_table[*id]);
}

void FSUB(pomp_parallel_fork)(int* id) {
  if ( pomp_tracing ) pomp_parallel_fork(pomp_rd_table[*id]);
}

void FSUB(pomp_parallel_join)(int* id) {
  if ( pomp_tracing ) pomp_parallel_join(pomp_rd_table[*id]);
}

void FSUB(pomp_section_begin)(int* id) {
  if ( pomp_tracing ) pomp_section_begin(pomp_rd_table[*id]);
}

void FSUB(pomp_section_end)(int* id) {
  if ( pomp_tracing ) pomp_section_end(pomp_rd_table[*id]);
}

void FSUB(pomp_sections_enter)(int* id) {
  if ( pomp_tracing ) pomp_sections_enter(pomp_rd_table[*id]);
}

void FSUB(pomp_sections_exit)(int* id) {
  if ( pomp_tracing ) pomp_sections_exit(pomp_rd_table[*id]);
}

void FSUB(pomp_single_begin)(int* id) {
  if ( pomp_tracing ) pomp_single_begin(pomp_rd_table[*id]);
}

void FSUB(pomp_single_end)(int* id) {
  if ( pomp_tracing ) pomp_single_end(pomp_rd_table[*id]);
}

void FSUB(pomp_single_enter)(int* id) {
  if ( pomp_tracing ) pomp_single_enter(pomp_rd_table[*id]);
}

void FSUB(pomp_single_exit)(int* id) {
  if ( pomp_tracing ) pomp_single_exit(pomp_rd_table[*id]);
}

void FSUB(pomp_workshare_enter)(int* id) {
  if ( pomp_tracing ) pomp_workshare_enter(pomp_rd_table[*id]);
}

void FSUB(pomp_workshare_exit)(int* id) {
  if ( pomp_tracing ) pomp_workshare_exit(pomp_rd_table[*id]);
}

void FSUB(pomp_begin)(int* id) {
  if ( pomp_tracing ) pomp_begin(pomp_rd_table[*id]);
}

void FSUB(pomp_end)(int* id) {
  if ( pomp_tracing ) pomp_end(pomp_rd_table[*id]);
}
