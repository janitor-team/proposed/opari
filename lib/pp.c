#include <stdio.h>

int main() {
  int i;

  #pragma omp parallel
    #pragma omp for
      for (i=0; i<4; ++i) printf("+++ pdo %d\n", i);
}
