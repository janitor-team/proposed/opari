/*************************************************************************/
/* OPARI Version 1.1                                                     */
/* Copyright (C) 2001                                                    */
/* Forschungszentrum Juelich, Zentralinstitut fuer Angewandte Mathematik */
/*************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#include "pomp_lib.h"

/*
 * Global variables
 */

int pomp_tracing = 0;

/*
 * C pomp function library
 */

void pomp_finalize() {
  static int pomp_finalize_called = 0;

  if ( ! pomp_finalize_called ) {
    pomp_finalize_called = 1;

    fprintf(stderr, "  0: finalize\n");
  }
}

void pomp_init() {
  int i;
  static int pomp_init_called = 0;

  if ( ! pomp_init_called ) {
    pomp_init_called = 1;

    atexit(pomp_finalize);
    fprintf(stderr, "  0: init\n");

    for(i=0; i<POMP_MAX_ID; ++i) {
      if ( pomp_rd_table[i] ) {
        pomp_rd_table[i]->data = 0;   /* <-- allocate space for
					     performance data here */
      }
    }
    pomp_tracing = 1;
  }
}

void pomp_off() {
  pomp_tracing = 0;
}

void pomp_on() {
  pomp_tracing = 1;
}

void pomp_atomic_enter(struct ompregdescr* r) {
  if ( pomp_tracing ) {
    fprintf(stderr, "%3d: enter atomic\n", omp_get_thread_num());
  }
}

void pomp_atomic_exit(struct ompregdescr* r) {
  if ( pomp_tracing ) {
    fprintf(stderr, "%3d: exit  atomic\n", omp_get_thread_num());
  }
}

void pomp_barrier_enter(struct ompregdescr* r) {
  if ( pomp_tracing ) {
    if ( r->name[0] == 'b' )
      fprintf(stderr, "%3d: enter barrier\n", omp_get_thread_num());
    else
      fprintf(stderr, "%3d: enter implicit barrier of %s\n",
	      omp_get_thread_num(), r->name);
  }
}

void pomp_barrier_exit(struct ompregdescr* r) {
  if ( pomp_tracing ) {
    if ( r->name[0] == 'b' )
      fprintf(stderr, "%3d: exit  barrier\n", omp_get_thread_num());
    else
      fprintf(stderr, "%3d: exit  implicit barrier of %s\n",
	      omp_get_thread_num(), r->name);
  }
}

void pomp_flush_enter(struct ompregdescr* r) {
  if ( pomp_tracing ) {
    fprintf(stderr, "%3d: enter flush\n", omp_get_thread_num());
  }
}

void pomp_flush_exit(struct ompregdescr* r) {
  if ( pomp_tracing ) {
    fprintf(stderr, "%3d: exit  flush\n", omp_get_thread_num());
  }
}

void pomp_critical_begin(struct ompregdescr* r) {
  if ( pomp_tracing ) {
    fprintf(stderr, "%3d: begin critical %s\n",
            omp_get_thread_num(), r->sub_name);
  }
}

void pomp_critical_end(struct ompregdescr* r) {
  if ( pomp_tracing ) {
    fprintf(stderr, "%3d: end   critical %s\n",
            omp_get_thread_num(), r->sub_name);
  }
}

void pomp_critical_enter(struct ompregdescr* r) {
  if ( pomp_tracing ) {
    fprintf(stderr, "%3d: enter critical %s\n",
            omp_get_thread_num(), r->sub_name);
  }
}

void pomp_critical_exit(struct ompregdescr* r) {
  if ( pomp_tracing ) {
    fprintf(stderr, "%3d: exit  critical %s\n",
            omp_get_thread_num(), r->sub_name);
  }
}

void pomp_for_enter(struct ompregdescr* r) {
  if ( pomp_tracing ) {
    fprintf(stderr, "%3d: enter for\n", omp_get_thread_num());
  }
}

void pomp_for_exit(struct ompregdescr* r) {
  if ( pomp_tracing ) {
    fprintf(stderr, "%3d: exit  for\n", omp_get_thread_num());
  }
}

void pomp_master_begin(struct ompregdescr* r) {
  if ( pomp_tracing ) {
    fprintf(stderr, "%3d: begin master\n", omp_get_thread_num());
  }
}

void pomp_master_end(struct ompregdescr* r) {
  if ( pomp_tracing ) {
    fprintf(stderr, "%3d: end   master\n", omp_get_thread_num());
  }
}

void pomp_parallel_begin(struct ompregdescr* r) {
  if ( pomp_tracing ) {
    fprintf(stderr, "%3d: begin parallel\n", omp_get_thread_num());
  }
}

void pomp_parallel_end(struct ompregdescr* r) {
  if ( pomp_tracing ) {
    fprintf(stderr, "%3d: end   parallel\n", omp_get_thread_num());
  }
}

void pomp_parallel_fork(struct ompregdescr* r) {
  if ( pomp_tracing ) {
    fprintf(stderr, "%3d: fork  parallel\n", omp_get_thread_num());
  }
}

void pomp_parallel_join(struct ompregdescr* r) {
  if ( pomp_tracing ) {
    fprintf(stderr, "%3d: join  parallel\n", omp_get_thread_num());
  }
}

void pomp_section_begin(struct ompregdescr* r) {
  if ( pomp_tracing ) {
    fprintf(stderr, "%3d: begin section\n", omp_get_thread_num());
  }
}

void pomp_section_end(struct ompregdescr* r) {
  if ( pomp_tracing ) {
    fprintf(stderr, "%3d: end   section\n", omp_get_thread_num());
  }
}

void pomp_sections_enter(struct ompregdescr* r) {
  if ( pomp_tracing ) {
    fprintf(stderr, "%3d: enter sections\n", omp_get_thread_num());
  }
}

void pomp_sections_exit(struct ompregdescr* r) {
  if ( pomp_tracing ) {
    fprintf(stderr, "%3d: exit  sections\n", omp_get_thread_num());
  }
}

void pomp_single_begin(struct ompregdescr* r) {
  if ( pomp_tracing ) {
    fprintf(stderr, "%3d: begin single\n", omp_get_thread_num());
  }
}

void pomp_single_end(struct ompregdescr* r) {
  if ( pomp_tracing ) {
    fprintf(stderr, "%3d: end   single\n", omp_get_thread_num());
  }
}

void pomp_single_enter(struct ompregdescr* r) {
  if ( pomp_tracing ) {
    fprintf(stderr, "%3d: enter single\n", omp_get_thread_num());
  }
}

void pomp_single_exit(struct ompregdescr* r) {
  if ( pomp_tracing ) {
    fprintf(stderr, "%3d: exit  single\n", omp_get_thread_num());
  }
}

void pomp_workshare_enter(struct ompregdescr* r) {
  if ( pomp_tracing ) {
    fprintf(stderr, "%3d: enter workshare\n", omp_get_thread_num());
  }
}

void pomp_workshare_exit(struct ompregdescr* r) {
  if ( pomp_tracing ) {
    fprintf(stderr, "%3d: exit  workshare\n", omp_get_thread_num());
  }
}

void pomp_begin(struct ompregdescr* r) {
  if ( pomp_tracing ) {
    fprintf(stderr, "%3d: begin region %s\n",
            omp_get_thread_num(), r->sub_name);
  }
}

void pomp_end(struct ompregdescr* r) {
  if ( pomp_tracing ) {
    fprintf(stderr, "%3d: end   region %s\n",
            omp_get_thread_num(), r->sub_name);
  }
}

void pomp_init_lock(omp_lock_t *s) {
  if ( pomp_tracing ) {
    fprintf(stderr, "%3d: init lock\n", omp_get_thread_num());
  }
  omp_init_lock(s);
}

void pomp_destroy_lock(omp_lock_t *s) {
  if ( pomp_tracing ) {
    fprintf(stderr, "%3d: destroy lock\n", omp_get_thread_num());
  }
  omp_destroy_lock(s);
}

void pomp_set_lock(omp_lock_t *s) {
  if ( pomp_tracing ) {
    fprintf(stderr, "%3d: set lock\n", omp_get_thread_num());
  }
  omp_set_lock(s);
}

void pomp_unset_lock(omp_lock_t *s) {
  if ( pomp_tracing ) {
    fprintf(stderr, "%3d: unset lock\n", omp_get_thread_num());
  }
  omp_unset_lock(s);
}

int  pomp_test_lock(omp_lock_t *s) {
  if ( pomp_tracing ) {
    fprintf(stderr, "%3d: test lock\n", omp_get_thread_num());
  }
  return omp_test_lock(s);
}

void pomp_init_nest_lock(omp_nest_lock_t *s) {
  if ( pomp_tracing ) {
    fprintf(stderr, "%3d: init nestlock\n", omp_get_thread_num());
  }
  omp_init_nest_lock(s);
}

void pomp_destroy_nest_lock(omp_nest_lock_t *s) {
  if ( pomp_tracing ) {
    fprintf(stderr, "%3d: destroy nestlock\n", omp_get_thread_num());
  }
  omp_destroy_nest_lock(s);
}

void pomp_set_nest_lock(omp_nest_lock_t *s) {
  if ( pomp_tracing ) {
    fprintf(stderr, "%3d: set nestlock\n", omp_get_thread_num());
  }
  omp_set_nest_lock(s);
}

void pomp_unset_nest_lock(omp_nest_lock_t *s) {
  if ( pomp_tracing ) {
    fprintf(stderr, "%3d: unset nestlock\n", omp_get_thread_num());
  }
  omp_unset_nest_lock(s);
}

int  pomp_test_nest_lock(omp_nest_lock_t *s) {
  if ( pomp_tracing ) {
    fprintf(stderr, "%3d: test nestlock\n", omp_get_thread_num());
  }
  return omp_test_nest_lock(s);
}

